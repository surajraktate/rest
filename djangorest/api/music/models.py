from django.db import models


# Create your models here.

class Songs(models.Model):
    title = models.CharField(max_length=250)
    artist = models.CharField(max_length=250)

    def __str__(self):
        return " ".join(self.title + " --> " + self.title)


class Employee(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    age = models.IntegerField()
    state = models.CharField(max_length=250)
    country = models.CharField(max_length=250)


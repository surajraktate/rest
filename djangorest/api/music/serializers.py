from rest_framework import serializers
from .models import Employee


class EmployeeSerializer(serializers.ModelSerializer):
    #status = StatusSerializer(many=True)
    class Meta:
        model = Employee
        #fields = ['id', 'name', 'age']
        #To Every Thing
        fields = ('__all__')
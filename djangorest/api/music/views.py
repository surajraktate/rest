from django.core.handlers import exception
from django.http import HttpResponse
from .models import Employee
from .serializers import EmployeeSerializer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics, status, request


from django.shortcuts import get_object_or_404



class EmployeeList(APIView):

    def get(self, request):
        #id = self.kwargs['id']
        #print("\n\n\n\n\n"+str(id)+"\n\n\n\n\n")
        id = int(request.GET.get('id', '0'))
        if int(id) == 0:
            employeeObj = Employee.objects.all()
        else:
            employeeObj = Employee.objects.all().filter(id=id)
            if len(employeeObj) == 0:
                return HttpResponse("<Employee Not Found>")

        serializer = EmployeeSerializer(employeeObj, many=True)

        newdict = {'Status': {"code": 200, "type": "success","message": "ok"}, 'result': serializer.data}


        new_serializer_data = list()
        new_serializer_data.append(newdict)

        # new_serializer_data.append({'result': serializer.data})

        print("here I am " + str(new_serializer_data))
        return Response(new_serializer_data)

    def post(self, request):
        serializer = EmployeeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        employee = self.get_object(pk)
        serializer = EmployeeSerializer(employee, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        employee = self.get_object(pk)
        employee.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#
# class EmployeeList(generics.ListAPIView):
#
#     serializer_class = EmployeeSerializer
#
#     def get_queryset(self):
#         print("\n\n\n\n\n"+request.query_params.get('id')+"\n\n\n\n\n")
#
# #
# class EmployeeDetail(generics.RetrieveUpdateDestroyAPIView):
#     queryset = Employee.objects.all()
#     serializer_class = EmployeeSerializer

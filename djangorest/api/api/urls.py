from django.contrib import admin
from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^Employee/$', include('music.urls')),
]

urlpatterns = format_suffix_patterns(urlpatterns)

# from rest_framework import routers
#
# router = routers.DefaultRouter()
# router.register(r'Employee', views.EmployeeList)
#
# # Wire up our API using automatic URL routing.
# # Additionally, we include login URLs for the browsable API.
# urlpatterns = [
#     url(r'^', include(router.urls)),
#     # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
# ]
